/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.beforemidterm;

/**
 *
 * @author hanam
 */
public class Reverse {

    public static void main(String[] args) {
        int[] A = {1,2,5,1,8};
        reverse(A);
        for (int i = 0; i < A.length; i++) {
            System.out.print(A[i]+" ");
        }
    }

    public static int[] reverse(int A[]) {
        int temp = 0;
        int index = 0;
        for (int i = A.length-1; i >= Math.floor(A.length / 2); i--) {
            temp = A[index];
            A[index] = A[i];
            A[i] = temp;
            index++;
        }
        return A;
    }
}


