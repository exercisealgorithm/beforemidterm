/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.beforemidterm;

/**
 *
 * @author hanam
 */
import java.util.Scanner;

public class LongestSubArray {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int A[] = {2, 3, 5, 3, 9, 2, 3, 5, 7, 6, 11, 13};
        int B[] = {5, 7, 69, 68, 154, 2, 7};

        sort(A);
    }

    public static void sort(int A[]) { 
        int count = 1;
        int result = 1;
        int index = 0;
        for (int i = 1; i < A.length; i++) {
            if (A[i] >= A[i - 1]) {
                count++; 
            } else if (count > result) {
                result = count; 
                index = i - result; 
                count = 1;
            } else if (count < result && i != A.length - 1 && A[i] < A[i - 1]) {
                count = 1;
            }
        }
        if (result < count) {
            result = count;
            index = A.length - result;
        }
        for (int i = index; i < result + index; i++) {
            System.out.print(A[i] + " ");
        }
    }

}
